using ChatSignalR.Helper;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddAllServices();

var app = builder.Build();

// app.MapGet("/", () => "Hello World!");

app.BuildAllApp();

app.Run();
