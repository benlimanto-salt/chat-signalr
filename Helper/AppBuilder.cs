using ChatSignalR.Hubs;

namespace ChatSignalR.Helper;

public static class AppBuilder
{
    public static WebApplication BuildAllApp(this WebApplication app)
    {
        // This is for All HTML Files
        app.UseDefaultFiles();
        app.UseStaticFiles();
        app.MapHub<ChatHubs>("/hub");

        return app;
    }
}