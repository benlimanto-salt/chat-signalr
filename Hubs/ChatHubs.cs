using ChatSignalR.Helper;
using Microsoft.AspNetCore.SignalR;

namespace ChatSignalR.Hubs;

public class ChatHubs : Hub
{
    private ListGroup _groups;

    public ChatHubs(ListGroup group)
    {
        _groups = group;
    }

    public async Task JoinGroup(string groupName, string username) 
    {
        _groups.AddGroup(groupName);
        _groups.AddUserToGroup(username, groupName);

        await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        await Clients.Group(groupName).SendAsync("sendGroup", Context.ConnectionId, $"{Context.ConnectionId} joinded the group!");
        await SendListGroups();
        return;
    }

    public async Task LeaveGroup(string groupName, string username)
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        _groups.RemoveUserFromGroup(username, groupName);
        var count = _groups.GetNumberOfUserInGroup(groupName);
        await Clients.Group(groupName).SendAsync("sendGroup", Context.ConnectionId, $"{Context.ConnectionId} has left the group {groupName}.");

        if (count <= 0)
        {
            _groups.RemoveGroup(groupName);
            await SendListGroups();
        }
    }

    public async Task SendChatGroup(string groupName, string msg)
    {
        await Clients.Group(groupName).SendAsync("sendGroup", Context.ConnectionId, $"{Context.ConnectionId}:{msg}");
    }

    public async Task SendListGroups()
    {
        await Clients.All.SendAsync("listGroups", _groups.ToString());
    }
}