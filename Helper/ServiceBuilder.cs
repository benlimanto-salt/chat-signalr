namespace ChatSignalR.Helper;

public static class ServiceBuilder
{
    public static IServiceCollection AddAllServices(this IServiceCollection srv)
    {
        srv.AddSignalR();
        srv.AddSingleton(typeof(ListGroup));
        return srv;
    }
}