import { Component, OnInit } from '@angular/core';
import * as SignalR from "@microsoft/signalr";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'client';
  public textBody = '<h1>Chat History</h1>';  

  private connection!: SignalR.HubConnection;
  public groupName?: string = undefined;
  public listGroup?: string = '';
  public txtChat!: string;
  public username!: string;

  ngOnInit(): void {
    this.connection = new SignalR.HubConnectionBuilder()
      .withUrl("/hub")
      .build();

    this.connection.on('sendGroup', (user, msg) => {
      this.onGroupMessage(user, msg);
    });

    this.connection.on('listGroups', (list) => { this.onListGroup(list); });

    this.connection.start().catch((err) => alert(err));
  }

  onGroupMessage(user: string, msg: string)
  {
    console.log(msg);
    console.log(this.textBody);
    this.textBody += "<span>"+msg+"</span><br/>";
  }

  onListGroup(list: string)
  {
    this.listGroup = list;
  }
  
  JoinGroup()
  {
    if(this.username == "" || this.username == undefined) 
    {
      alert("KOSONG!");
      return;
    }

    this.groupName = "botak";
    this.connection.send("JoinGroup", "botak", this.username);
  }

  LeaveGroup()
  {
    this.connection.send("LeaveGroup", this.groupName, this.username);
    this.textBody = '<h3>Chat History</h3>';
    this.groupName = undefined;
  }

  SendMsg(e: Event)
  {
    e.preventDefault();
    this.connection.send("SendChatGroup", this.groupName, this.txtChat);
    this.txtChat = "";
  }
}
