namespace ChatSignalR.Helper;

public class ListGroup 
{
    private List<string> _group = new List<string>();
    private Dictionary<string,List<string>> _users = new Dictionary<string, List<string>>();

    public void AddGroup(string groupName)
    {
        if (_group.Where(q => q == groupName).Count() > 0) return;

        _group.Add(groupName.ToLower());
        _users.Add(groupName, new List<string>());
    }

    public void RemoveGroup(string groupName)
    {
        var gc = _group.Where(q => q == groupName);
        if (gc.Count() <= 0) return;
        
        _group.Remove(groupName);
        _users.Remove(groupName);
    }

    public override string ToString()
    {
        string list = "";
        
        foreach (var l in _group)
        {
            list += l + ",";
        }
        if (list == "") return "";
        return list.Substring(0, list.Length - 1);
    }

    public void AddUserToGroup(string user, string group)
    {
        if (_users[group].Where(q => q == user.ToLower()).Count() > 0) return;

        _users[group].Add(user.ToLower());
    }

    public void RemoveUserFromGroup(string user, string group)
    {
        if (_users[group].Where(q => q == user.ToLower()).Count() <= 0) return;

        _users[group].Remove(user.ToLower());
    }

    public int GetNumberOfUserInGroup(string group)
    {
        if (_users.Where(q => q.Key == group).Count() <= 0) return -1;
        return _users[group].Count();
    }
}